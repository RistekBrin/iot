<?php

namespace App\Http\Controllers;

use App\Models\DetailEngine;
use Illuminate\Http\Request;

class DetailEngineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetailEngine  $detailEngine
     * @return \Illuminate\Http\Response
     */
    public function show(DetailEngine $detailEngine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetailEngine  $detailEngine
     * @return \Illuminate\Http\Response
     */
    public function edit(DetailEngine $detailEngine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetailEngine  $detailEngine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailEngine $detailEngine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetailEngine  $detailEngine
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailEngine $detailEngine)
    {
        //
    }
}
