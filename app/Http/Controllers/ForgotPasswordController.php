<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;

class ForgotPasswordController extends Controller
{
    /**
     * Display register page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('auth.formforgotpassword');
    }

    /**
     * Handle account forgotpassword request
     *
     * @param ForgotPasswordRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function forgotpassword(forgotPasswordRequest $request)
    {

    }
}
