<?php

namespace App\Http\Controllers;

use App\Models\DetailEngine;
use App\Models\Engine;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('landing.landing-master');
    }
    public function home()
    {
        try {
            $detailEngine = DetailEngine::latest()->first();
            $engine = Engine::all();
            return view('home.home',['detailEngine' => $detailEngine, 'engine' => $engine]);
            
        } catch (\Throwable $e) {
            return response()->json([
                'message' => 'Gagal Mengambil Data'
            ]);
        }
    }
}
