<?php

namespace App\Http\Controllers;

use App\Models\DataDetail;
use Illuminate\Http\Request;

class DataDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.datadetail');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DataDetail  $dataDetail
     * @return \Illuminate\Http\Response
     */
    public function show(DataDetail $dataDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DataDetail  $dataDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(DataDetail $dataDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DataDetail  $dataDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataDetail $dataDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DataDetail  $dataDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataDetail $dataDetail)
    {
        //
    }
}
