<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class insertmessage extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'dataku';
    protected $fillable = [
        'data',
        'created_at',
        'updated_at'
    ];
}
