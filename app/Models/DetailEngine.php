<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailEngine extends Model
{
    use HasFactory;
    protected $primaryKey = 'idDataEngine';
    protected $table = 'detail_engines';
    protected $fillable = [
        'idDataEngine',
        'idEngine',
        'terminalTime',
        'groupName',
        'voltageRS',
        'voltageST',
        'voltageTR',
        'voltageRN',
        'voltageSN',
        'voltageTN',
        'currentR',
        'currentS',
        'currentT',
        'powerActive',
        'powerReactive',
        'powerApparent',
        'powerFactor',
        'Frequency',
        'peakDemandActive',
        'peakDemandReactive',
        'peakDemandApparent',
        'accumulatedEnergy',
        'created_at',
        'updated_at'
    ];

    public function refengines()
    {
        return $this->belongsTo(Engine::class, 'idEngine');
    }


}
