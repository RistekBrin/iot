<?php

namespace App\Console\Commands;

use App\Models\DetailEngine;
use Illuminate\Console\Command;
use PhpMqtt\Client\Facades\MQTT;

class Subscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mqtt:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe To MQTT topic';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Subscribe From Topik /Engine 1
        $mqtt = MQTT::connection();
        $mqtt->subscribe('data/MDP1Taco/HMIMDPTaco/+', function(string $topic, string $message) { 
            echo $message;
            $dataku = $message;
            $data = json_decode($dataku, true);
            //print_r($data);
            try {
                $simpan = new DetailEngine;
                $simpan->idEngine = 1;
                $simpan->terminalTime = $data['_terminalTime'];
                $simpan->groupName = $data['_groupName'];
                $simpan->voltageRS = $data['Voltage_R_S'];
                $simpan->voltageST = $data['Voltage_R_S'];
                $simpan->voltageTR = $data['Voltage_R_S'];
                $simpan->voltageRN = $data['Voltage_R_S'];
                $simpan->voltageSN = $data['Voltage_R_S'];
                $simpan->voltageTN = $data['Voltage_R_S'];
                $simpan->currentR = $data['Current_R'];
                $simpan->currentS = $data['Current_R'];
                $simpan->currentT = $data['Current_R'];
                $simpan->powerActive = $data['Power_Active'];
                $simpan->powerReactive = $data['Power_Reactive'];
                $simpan->powerApparent = $data['Power_Apparent'];
                $simpan->powerFactor = $data['Power_Factor'];
                $simpan->frequency = $data['Frequency'];
                $simpan->peakDemandActive = $data['Peak_Demand_Active'];
                $simpan->peakDemandReactive = $data['Peak_Demand_Reactive'];
                $simpan->peakDemandApparent = $data['Peak_Demand_Apparent'];
                $simpan->accumulatedEnergy = $data['Accumulated_Energy'];
                $simpan->save();

                return response()->json([
                    'message' => 'Data Tersimpan Dengan Baik',
                ], 201);
            } catch (\Throwable $e) {
                return response()->json([
                    'message' => 'Gagal Tersimpan',
                    'detail' => $e.getMessage(),
                ]);
            }
            $mqtt->interrupt();
        });
        
        $mqtt->loop(true);
        
        return Command::SUCCESS;
    }
}
