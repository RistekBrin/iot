<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_engines', function (Blueprint $table) {
            $table->uuid('idDataEngine')->default(DB::raw('uuid_generate_v4()'));
            $table->integer('idEngine')->nullable();
            $table->date('terminalTime')->nullable();
            $table->string('groupName')->nullable();
            $table->float('voltageRS')->nullable();
            $table->float('voltageST')->nullable();
            $table->float('voltageTR')->nullable();
            $table->float('voltageRN')->nullable();
            $table->float('voltageSN')->nullable();
            $table->float('voltageTN')->nullable();
            $table->float('currentR')->nullable();
            $table->float('currentS')->nullable();
            $table->float('currentT')->nullable();
            $table->float('powerActive')->nullable();
            $table->float('powerReactive')->nullable();
            $table->float('powerApparent')->nullable();
            $table->float('powerFactor')->nullable();
            $table->float('frequency')->nullable();
            $table->float('peakDemandActive')->nullable();
            $table->float('peakDemandReactive')->nullable();
            $table->float('peakDemandApparent')->nullable();
            $table->float('accumulatedEnergy')->nullable();
            $table->timestamps();
            
        });
        
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_engines');
    }
};
