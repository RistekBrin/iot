<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Engine;
use Illuminate\Support\Facades\DB;

class dataEngine extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        [
            'idEngine' => '1',
            'namaEngine' => 'Boiler',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ],[
            'idEngine' => '2',
            'namaEngine' => 'Sanding',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ],[
            'idEngine' => '3',
            'namaEngine' => 'Cutting',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ],[
            'idEngine' => '4',
            'namaEngine' => 'Press',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ],[
            'idEngine' => '5',
            'namaEngine' => 'Treating',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ],[
            'idEngine' => '6',
            'namaEngine' => 'Chemical',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ],[
            'idEngine' => '7',
            'namaEngine' => 'Compressor',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ],[
            'idEngine' => '8',
            'namaEngine' => 'MDP',
            'created_at' => new \DateTime,
            'updated_at' => new \DateTime,
        ]
    ];
        DB::table('engines')->delete();
        \DB::table('engines')->insert($data);
    }
}
