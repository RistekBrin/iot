@extends('layouts.auth')
@section('content')
<!--begin::Body-->
<div class="d-flex flex-column flex-lg-row-fluid py-10">
<!--begin::Content-->
<div class="d-flex flex-center flex-column flex-column-fluid">
    <!--begin::Wrapper-->
    <div class="w-lg-600px p-10 p-lg-15 mx-auto">
        <!--begin::Form-->
        <form class="form w-100" method="post" action="{{ route('register.perform') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <!--begin::Heading-->
            <div class="mb-10 text-center">
                <!--begin::Title-->
                <h1 class="text-dark mb-3">Create an Account</h1>
                <!--end::Title-->
                <!--begin::Link-->
                <div class="text-gray-400 fw-bold fs-4">Already have an account?
                <a href="/login" class="link-primary fw-bolder">Sign in here</a></div>
                <!--end::Link-->
            </div>
            <!--end::Heading-->
             <!--begin::Input group-->
             <div class="fv-row mb-7">
                <label class="form-label fw-bolder text-dark fs-6">Email</label>
                <input class="form-control form-control-lg form-control-solid" type="email" value="{{ old('email') }}" placeholder="name@example.com" name="email" required="required" autofocus />
            </div>
            <!--end::Input group-->
            @if ($errors->has('email'))
                <span class="text-danger text-left">{{ $errors->first('email') }}</span>
            @endif
            <!--begin::Input group-->
            <div class="fv-row mb-7">
                    <label class="form-label fw-bolder text-dark fs-6">Username</label>
                    <input class="form-control form-control-lg form-control-solid" type="text" value="{{ old('username') }}" placeholder="Username" name="username" required="required" />
            </div>
            <!--end::Input group-->
            @if ($errors->has('username'))
                <span class="text-danger text-left">{{ $errors->first('username') }}</span>
            @endif
            <!--begin::Input group-->
            <div class="mb-10 fv-row" data-kt-password-meter="true">
                <!--begin::Wrapper-->
                <div class="mb-1">
                    <!--begin::Label-->
                    <label class="form-label fw-bolder text-dark fs-6">Password</label>
                    <!--end::Label-->
                    <!--begin::Input wrapper-->
                    <div class="position-relative mb-3">
                        <input class="form-control form-control-lg form-control-solid" type="password" value="{{ old('password') }}" placeholder="Password"  name="password" required="required" />
                        <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                            <i class="bi bi-eye-slash fs-2"></i>
                            <i class="bi bi-eye fs-2 d-none"></i>
                        </span>
                    </div>
                    <!--end::Input wrapper-->
                    @if ($errors->has('password'))
                    <span class="text-danger text-left">{{ $errors->first('password') }}</span>
                    @endif
                    <!--begin::Meter-->
                    <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                    </div>
                    <!--end::Meter-->
                </div>
                <!--end::Wrapper-->
                <!--begin::Hint-->
                <div class="text-muted">Use 8 or more characters with a mix of letters, numbers &amp; symbols.</div>
                <!--end::Hint-->
            </div>
            <!--end::Input group=-->
            <!--begin::Input group-->
            <div class="fv-row mb-5">
                <label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
                <input class="form-control form-control-lg form-control-solid" type="password" placeholder="Confirm Password" name="password_confirmation" value="{{ old('password_confirmation') }}" required="required" />
            </div>
            <!--end::Input group-->

            <!--begin::Actions-->
            <div class="d-flex flex-wrap justify-content-center pb-lg-0">
                <button type="submit" class="btn btn-lg btn-primary fw-bolder me-4">
                    <span class="indicator-label">Register</span>
                </button>
                <a href="{{ route('login.perform') }}" class="btn btn-lg btn-light-primary fw-bolder">Cancel</a>
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
    <!--end::Wrapper-->
</div>
<!--end::Content-->
<!--begin::Footer-->
<div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
    <!--begin::Links-->
    <div class="d-flex flex-center fw-bold fs-6">
         @include('auth.partials.copy')
    </div>
    <!--end::Links-->
</div>
<!--end::Footer-->
</div>
<!--end::Body-->
@endsection
