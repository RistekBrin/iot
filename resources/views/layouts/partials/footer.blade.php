<!--begin::Copyright-->
	<div class="text-dark order-2 order-md-1">
	<span class="text-muted fw-bold me-1">{{date('Y')}} &copy; </span>
	<a href="https://taco.co.id" target="_blank" class="text-gray-800 text-hover-primary">PT.Taco,tbk</a>
	</div>
<!--end::Copyright-->