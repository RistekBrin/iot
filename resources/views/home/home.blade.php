@extends('layouts.master')

@section('content')
    @auth
        				<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Boiler A</h1>
									<!--end::Title-->
									<!--begin::Separator-->
									<span class="h-20px border-gray-300 border-start mx-4"></span>
									<!--end::Separator-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center py-1">
								<!--begin::Item Energy-->
								<div class="d-flex align-items-center bg-light-primary rounded p-2 fs-3 me-3">
									<!--begin::Icon-->
									<span class="svg-icon svg-icon-gray me-3">
										<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg-->
										<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
										<path d="M7.5 1v7h1V1h-1z"/>
										<path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
										</svg>
										</span>
										<!--end::Svg Icon-->
									</span>
									<!--end::Icon-->
									<!--begin::Title-->
									<div class="flex-grow-1 me-2">
										<span class="fw-bolder text-gray-800 fs-2">Energy</span>
										<span class="text-muted d-block fs-7">Device</span>
									</div>
									<!--end::Title-->
									&nbsp;&nbsp;
									<!--begin::Lable-->
									<div class="flex-grow-1 me-2">
										<span class="fw-bolder text-primary py-1">{{$detailEngine->accumulatedEnergy ?? '0'}}</span>
										<span class="fw-bolder d-block fs-7">kwh</span>
									</div>
									<!--end::Lable-->
								</div>
								<!--end::Item Energy-->

								<!--begin::Item Connect-->
								<div class="d-flex align-items-center bg-light-success rounded p-2 fs-3 me-3">
									<!--begin::Icon-->
									<span class="svg-icon svg-icon-gray me-3">
										<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg-->
										<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plug-fill" viewBox="0 0 16 16">
										<path d="M6 0a.5.5 0 0 1 .5.5V3h3V.5a.5.5 0 0 1 1 0V3h1a.5.5 0 0 1 .5.5v3A3.5 3.5 0 0 1 8.5 10c-.002.434-.01.845-.04 1.22-.041.514-.126 1.003-.317 1.424a2.083 2.083 0 0 1-.97 1.028C6.725 13.9 6.169 14 5.5 14c-.998 0-1.61.33-1.974.718A1.922 1.922 0 0 0 3 16H2c0-.616.232-1.367.797-1.968C3.374 13.42 4.261 13 5.5 13c.581 0 .962-.088 1.218-.219.241-.123.4-.3.514-.55.121-.266.193-.621.23-1.09.027-.34.035-.718.037-1.141A3.5 3.5 0 0 1 4 6.5v-3a.5.5 0 0 1 .5-.5h1V.5A.5.5 0 0 1 6 0z"/>
										</svg>
										</svg>
										</span>
										<!--end::Svg Icon-->
									</span>
									<!--end::Icon-->
									<!--begin::Title-->
									<div class="flex-grow-1 me-2">
										<span class="fw-bolder text-gray-800 fs-2">Connect</span>
										<span class="text-muted d-block fs-7">Device</span>
									</div>
									<!--end::Title-->
									&nbsp;&nbsp;
									<!--begin::Lable-->
									<div class="flex-grow-1 me-2">
										<span class="fw-bolder text-success py-1 fs2">0</span>
									</div>
									<!--end::Lable-->
								</div>
								<!--end::Item Connect-->
								<!--begin::Item Disconnect-->
								<div class="d-flex align-items-center bg-light-danger rounded p-2 fs-3">
									<!--begin::Icon-->
									<span class="svg-icon svg-icon-gray me-3">
										<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg-->
										<span class="svg-icon svg-icon-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plugin" viewBox="0 0 16 16">
										<path fill-rule="evenodd" d="M1 8a7 7 0 1 1 2.898 5.673c-.167-.121-.216-.406-.002-.62l1.8-1.8a3.5 3.5 0 0 0 4.572-.328l1.414-1.415a.5.5 0 0 0 0-.707l-.707-.707 1.559-1.563a.5.5 0 1 0-.708-.706l-1.559 1.562-1.414-1.414 1.56-1.562a.5.5 0 1 0-.707-.706l-1.56 1.56-.707-.706a.5.5 0 0 0-.707 0L5.318 5.975a3.5 3.5 0 0 0-.328 4.571l-1.8 1.8c-.58.58-.62 1.6.121 2.137A8 8 0 1 0 0 8a.5.5 0 0 0 1 0Z"/>
										</svg>
										</span>
										<!--end::Svg Icon-->
									</span>
									<!--end::Icon-->
									<!--begin::Title-->
									<div class="flex-grow-1 me-2">
										<span class="fw-bolder text-gray-800 fs-2">Disconnect</span>
										<span class="text-muted d-block fs-7">Device</span>
									</div>
									<!--end::Title-->
									&nbsp;&nbsp;
									<!--begin::Lable-->
									<div class="flex-grow-1 me-2">
										<span class="fw-bolder text-danger py-1 fs2">0</span>
									</div>
									<!--end::Lable-->
								</div>
								<!--end::Item Energy-->
								</div>
								<!--begin::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
								<!--begin::Row-->
								<div class="row gy-5 g-xl-8">
									<!--begin::Col-->
									<div class="col-xl-4">
										<!--begin::List Widget 6-->
										<div class="card card-xl-stretch mb-xl-8">
											<!--begin::Header-->
											<div class="card-header border-0">
												<h3 class="card-title fw-bolder text-dark">Power Supply</h3>
												<h3 class="card-title fw-bolder text-dark">Peak Demand</h3>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body pt-0">
												<!--begin row-->
												<div class="row gy-5 g-xl-8">
													<!--begin::col-->
													<div class="col py-8 rounded-2 me-0">
														<!--begin::Item-->
														<div class="d-flex align-items-center bg-light-info rounded p-2 fs-3 me-3">
															<!--begin::Title-->
															<div class="flex-grow-1 me-2">
																<center>
																<span class="text-muted d-block fs-7">Active</span>&nbsp;
																<span class="fw-bolder text-bold py-1">{{$detailEngine->powerActive ?? '0'}} kW</span>
																</center>
															</div>
															<!--end::Title-->
														</div>
														<!--end::Item-->
													</div>
													<!--end::col-->
													<!--begin::col-->
													<div class="col py-8 rounded-2 me-0">
														<!--begin::Item-->
														<div class="d-flex align-items-center bg-light-info rounded p-2 fs-3 me-3">
															<!--begin::Title-->
															<div class="flex-grow-1 me-2">
																<center>
																<span class="text-muted d-block fs-7">Active</span>&nbsp;
																<span class="fw-bolder text-bold py-1">{{$detailEngine->peakDemandActive ?? '0'}} kW</span>
																</center>
															</div>
															<!--end::Title-->
														</div>
														<!--end::Item-->
													</div>
													<!--end::col-->
												</div>
												<!--end::row-->
												<!--begin row-->
												<div class="row gy-5 g-xl-8">
													<!--begin::col-->
													<div class="col py-8 rounded-2 me-0">
														<!--begin::Item-->
														<div class="d-flex align-items-center bg-light-danger rounded p-2 fs-3 me-3">
															<!--begin::Title-->
															<div class="flex-grow-1 me-2">
																<center>
																<span class="text-muted d-block fs-7">Reactive</span>&nbsp;
																<span class="fw-bolder text-bold py-1">{{$detailEngine->powerReactive ?? '0'}} kVAR</span>
																</center>
															</div>
															<!--end::Title-->
														</div>
														<!--end::Item-->
													</div>
													<!--end::col-->
													<!--begin::col-->
													<div class="col py-8 rounded-2 me-0">
														<!--begin::Item-->
														<div class="d-flex align-items-center bg-light-danger rounded p-2 fs-3 me-3">
															<!--begin::Title-->
															<div class="flex-grow-1 me-2">
																<center>
																<span class="text-muted d-block fs-7">Reactive</span>&nbsp;
																<span class="fw-bolder text-bold py-1">{{$detailEngine->peakDemandReactive ?? '0'}} kVAR</span>
																</center>
															</div>
															<!--end::Title-->
														</div>
														<!--end::Item-->
													</div>
													<!--end::col-->
												</div>
												<!--end::row-->
												<!--begin row-->
												<div class="row gy-5 g-xl-8">
													<!--begin::col-->
													<div class="col py-8 rounded-2 me-0">
														<!--begin::Item-->
														<div class="d-flex align-items-center bg-light-warning rounded p-2 fs-3 me-3">
															<!--begin::Title-->
															<div class="flex-grow-1 me-2">
																<center>
																<span class="text-muted d-block fs-7">Apparent</span>&nbsp;
																<span class="fw-bolder text-bold py-1">{{$detailEngine->powerApparent ?? '0'}} kVA</span>
																</center>
															</div>
															<!--end::Title-->
														</div>
														<!--end::Item-->
													</div>
													<!--end::col-->
													<!--begin::col-->
													<div class="col py-8 rounded-2 me-0">
														<!--begin::Item-->
														<div class="d-flex align-items-center bg-light-warning rounded p-2 fs-3 me-3">
															<!--begin::Title-->
															<div class="flex-grow-1 me-2">
																<center>
																<span class="text-muted d-block fs-7">Apparent</span>&nbsp;
																<span class="fw-bolder text-bold py-1">{{$detailEngine->peakDemandApparent ?? '0'}} kVA</span>
																</center>
															</div>
															<!--end::Title-->
														</div>
														<!--end::Item-->
													</div>
													<!--end::col-->
												</div>
												<!--end::row-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::List Widget 6-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-5">
										<!--begin::List Widget 5-->
										<div class="card card-xl-stretch">
											<!--begin::Body-->
											<div class="card-body pt-8">
												<!--begin::Item-->
												<div class="d-flex align-items-center bg-light-primary rounded p-5 mb-7">
													<!--begin::Icon-->
													<span class="svg-icon svg-icon-info me-5">
														<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg-->
														<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lightning-fill" viewBox="0 0 16 16">
														<path d="M5.52.359A.5.5 0 0 1 6 0h4a.5.5 0 0 1 .474.658L8.694 6H12.5a.5.5 0 0 1 .395.807l-7 9a.5.5 0 0 1-.873-.454L6.823 9.5H3.5a.5.5 0 0 1-.48-.641l2.5-8.5z"/>
														</svg>
														</span>
														<!--end::Svg Icon-->
													</span>
													<!--end::Icon-->
													<!--begin::R-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">R</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->currentR ?? '0'}}</span>
														<span class="text-muted fs-6">Ampere</span>
														</center>
													</div>
													<!--end::R-->
													<!--begin::Separator-->
													<span class="h-40px border-gray-300 border-start mx-4"></span>
													<!--end::Separator-->
													<!--begin::S-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">S</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->currentS ?? '0'}}</span>
														<span class="text-muted fs-6">Ampere</span>
														</center>
													</div>
													<!--end::S-->
													<!--begin::Separator-->
													<span class="h-40px border-gray-300 border-start mx-4"></span>
													<!--end::Separator-->
													<!--begin::T-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">T</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->currentT ?? '0'}}</span>
														<span class="text-muted fs-6">Ampere</span>
														</center>
													</div>
													<!--end::T-->
												</div>
												<!--end::Item-->
											</div>
											<!--end::Body-->
											<center>
											<span class="fw-bolder text-gray-800 fs-6 align-items-center">Voltage</span>
											<span class="text-muted fs-6 align-items-center">L-L (RST)</span>
											</center>
											<!--begin::Body-->
											<div class="card-body pt-0">
												<!--begin::Item-->
												<div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
													<!--begin::Icon-->
													<span class="svg-icon svg-icon-info me-5">
														<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg-->
														<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lightning-fill" viewBox="0 0 16 16">
														<path d="M5.52.359A.5.5 0 0 1 6 0h4a.5.5 0 0 1 .474.658L8.694 6H12.5a.5.5 0 0 1 .395.807l-7 9a.5.5 0 0 1-.873-.454L6.823 9.5H3.5a.5.5 0 0 1-.48-.641l2.5-8.5z"/>
														</svg>
														</span>
														<!--end::Svg Icon-->
													</span>
													<!--end::Icon-->
													<!--begin::R-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">RS</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->voltageRS ?? '0'}}</span>
														<span class="text-muted fs-6">Volt</span>
														</center>
													</div>
													<!--end::R-->
													<!--begin::Separator-->
													<span class="h-40px border-gray-300 border-start mx-4"></span>
													<!--end::Separator-->
													<!--begin::S-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">ST</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->voltageST ?? '0'}}</span>
														<span class="text-muted fs-6">Volt</span>
														</center>
													</div>
													<!--end::S-->
													<!--begin::Separator-->
													<span class="h-40px border-gray-300 border-start mx-4"></span>
													<!--end::Separator-->
													<!--begin::T-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">TR</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->voltageTR ?? '0'}}</span>
														<span class="text-muted fs-6">Volt</span>
														</center>
													</div>
													<!--end::T-->
												</div>
												<!--end::Item-->
											</div>
											<!--end::Body-->
											<center>
											<span class="fw-bolder text-gray-800 fs-6 align-items-center">Voltage</span>
											<span class="text-muted fs-6 align-items-center">L-N (RST)</span>
											</center>
											<!--begin::Body-->
											<div class="card-body pt-0">
												<!--begin::Item-->
												<div class="d-flex align-items-center bg-light-warning rounded p-5 mb-7">
													<!--begin::Icon-->
													<span class="svg-icon svg-icon-info me-5">
														<!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg-->
														<span class="svg-icon svg-icon-1">
														<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-lightning-fill" viewBox="0 0 16 16">
														<path d="M5.52.359A.5.5 0 0 1 6 0h4a.5.5 0 0 1 .474.658L8.694 6H12.5a.5.5 0 0 1 .395.807l-7 9a.5.5 0 0 1-.873-.454L6.823 9.5H3.5a.5.5 0 0 1-.48-.641l2.5-8.5z"/>
														</svg>
														</span>
														<!--end::Svg Icon-->
													</span>
													<!--end::Icon-->
													<!--begin::R-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">RN</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->voltageRN ?? '0'}}</span>
														<span class="text-muted fs-6">Volt</span>
														</center>
													</div>
													<!--end::R-->
													<!--begin::Separator-->
													<span class="h-40px border-gray-300 border-start mx-4"></span>
													<!--end::Separator-->
													<!--begin::S-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">SN</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->voltageSN ?? '0'}}</span>
														<span class="text-muted fs-6">Volt</span>
														</center>
													</div>
													<!--end::S-->
													<!--begin::Separator-->
													<span class="h-40px border-gray-300 border-start mx-4"></span>
													<!--end::Separator-->
													<!--begin::T-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">TN</a>
														<span class="fw-bolder text-gray-800 fs-1 d-block">{{$detailEngine->voltageTN ?? '0'}}</span>
														<span class="text-muted fs-6">Volt</span>
														</center>
													</div>
													<!--end::T-->
												</div>
												<!--end::Item-->
											</div>
											<!--end::Body-->
										</div>
										<!--end: List Widget 5-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-3">
										<!--begin::Mixed Widget 7-->
										<div class="card card-xl-stretch-20 mb-5 mb-xl-8">
											<!--begin::Body-->
											<div class="card-body pt-8">
												<!--begin::Item-->
												<div class="d-flex align-items-center bg-light-danger rounded p-5 mb-7">
													<!--begin::R-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">P. Factor</a>
														<span class="fw-bolder text-gray-800 fs-3 d-block">{{$detailEngine->powerFactor ?? '0'}}</span>
														</center>
													</div>
													<!--end::R-->
													<!--begin::Separator-->
													<span class="h-40px border-gray-300 border-start mx-4"></span>
													<!--end::Separator-->
													<!--begin::S-->
													<div class="flex-grow-1 me-2">
														<center>
														<a href="#" class="text-muted  fs-6">Frequency</a>
														<span class="fw-bolder text-gray-800 fs-3 d-block">{{$detailEngine->frequency ?? '0'}} Hz</span>
														</center>
													</div>
													<!--end::S-->
												
												</div>
												<!--end::Item-->
											</div>
											<!--end::Body-->
										</div>
										<!--end::Mixed Widget 7-->
										<!--begin::Mixed Widget 10-->
										<div class="card card-xl-stretch-90 mb-5 mb-xl-8">
											<!--begin::Body-->
											<div class="card-body d-flex flex-column p-0">
												<!--begin::Hidden-->
												<div class="d-flex flex-stack flex-wrap flex-grow-1 px-9 pt-9 pb-3">
													<div class="me-2">
														<span class="fw-bolder text-gray-800 d-block fs-3">Accumulate Energy</span>
													</div>
													<div class="card-toolbar">
													<!--begin::Menu-->
													<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
														<!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
														<span class="svg-icon svg-icon-2">
															<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
																	<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																	<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																	<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																</g>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</button>
													<!--begin::Menu 3-->
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
														<!--begin::Heading-->
														<div class="menu-item px-3">
															<div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Filter Data</div>
														</div>
														<!--end::Heading-->
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Last 10 Hours</a>
														</div>
														<!--end::Menu item-->
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Last 20 Hours</a>
														</div>
														<!--end::Menu item-->
													</div>
													<!--end::Menu 3-->
													<!--end::Menu-->
												</div>
												</div>
												<!--end::Hidden-->
												<!--begin::Chart-->
												<div id="oee1" data-kt-color="primary" style="height: 200px"></div>
												<!--end::Chart-->
												<!--begin::Chart-->
												<div class="mixed-widget-7-chart card-rounded-bottom" data-kt-chart-color="primary" style="height: 100px"></div>
												<!--end::Chart-->
											</div>
										</div>
										<!--end::Mixed Widget 10-->
									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row gy-5 g-xl-8">
										<!--begin::Tables Widget 9-->
										<div class="card card-xl-stretch-50 mb-5 mb-xl-8">
											<!--begin::Header-->
											<div class="card-header border-0 pt-5">
												<h3 class="card-title align-items-start flex-column">
													<span class="card-label fw-bolder fs-3 mb-1">Trend Peak Demand</span>
												</h3>
												<div class="card-toolbar">
													<!--begin::Menu-->
													<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
														<!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
														<span class="svg-icon svg-icon-2">
															<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
																	<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																	<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																	<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																</g>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</button>
													<!--begin::Menu 3-->
													<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
														<!--begin::Heading-->
														<div class="menu-item px-3">
															<div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Filter Data</div>
														</div>
														<!--end::Heading-->
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Last 10 Hours</a>
														</div>
														<!--end::Menu item-->
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-3">Last 20 Hours</a>
														</div>
														<!--end::Menu item-->
													</div>
													<!--end::Menu 3-->
													<!--end::Menu-->
												</div>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body py-3">
												<!--begin::Chart-->
												<div id="graphpeakdeman"  style="height: 400px"></div>
												<!--end::Chart-->
											</div>
											<!--begin::Body-->
										</div>
										<!--end::Tables Widget 9-->
								</div>
								<!--end::Row-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
    @endauth
@endsection