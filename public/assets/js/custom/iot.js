"use strict"
//Oee
var chartDom1 = document.getElementById('oee1');
var myChart1 = echarts.init(chartDom1);
var option;

const gaugeData = [
  {
    
    value: 10,
    name: '',
    title: {
      offsetCenter: ['0%', '-0%']
    },
    detail: {
      valueAnimation: true,
      offsetCenter: ['0%', '-0%']
    }
  }
 
];
option = {
  series: [
    {
      type: 'gauge',
      startAngle: 90,
      endAngle: -270,
      color:'#6926fc',
      pointer: {
        show: false
      },
      progress: {
        show: true,
        overlap: false,
        roundCap: true,
        clip: false,
        itemStyle: {
          borderWidth: 1,
          borderColor: '#ffffff'
        }
      },
      axisLine: {
        lineStyle: {
          width: 20
          
        }
      },
      splitLine: {
        show: false,
        distance: 0,
        length: 10
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        show: false,
        distance: 50
      },
      data: gaugeData,
      title: {
        fontSize: 14
      },
      detail: {
        width: 50,
        height: 14,
        fontSize: 20,
        color: 'inherit',
        formatter: '{value}Kwh'
      }
    }
  ]
};
// setInterval(function () {
//   gaugeData[0].value = +(Math.random() * 1000).toFixed(2);
//   myChart1.setOption({
//     series: [
//       {
//         data: gaugeData,
//         pointer: {
//           show: false
//         }
//       }
//     ]
//   });
// }, 2000);

option && myChart1.setOption(option);

var chartDom2 = document.getElementById('graphpeakdeman');
var myChart2 = echarts.init(chartDom2);
var option1;

option1 = {
  title: {
    text: ''
  },
  tooltip: {
    trigger: 'axis'
  },
  legend: {
    data: ['Active', 'Reactive', 'Apparent']
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  toolbox: {
    feature: {
      saveAsImage: {}
    }
  },
  xAxis: {
    type: 'category',
    boundaryGap: false,
    data: ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24']
  },
  yAxis: {
    type: 'value'
  },
  series: [
    {
      name: 'Active',
      type: 'line',
      stack: 'Total',
      data: [120, 132, 101, 134, 90, 230, 210,120, 132, 101, 134, 90, 230, 210,120, 132, 101, 134, 90, 230, 210,134, 90, 230, 210]
    },
    {
      name: 'Reactive',
      type: 'line',
      stack: 'Total',
      data: [220, 182, 191, 234, 290, 330, 310,182, 191, 234, 290, 330, 310,220, 182, 191, 234, 290, 330, 310,182,330, 310,182,190]
    },
    {
      name: 'Apparent',
      type: 'line',
      stack: 'Total',
      data: [150, 232, 201, 154, 190, 330, 410,150, 232, 201, 154, 190, 330, 410,150, 232, 201, 154, 190, 330, 410,150, 232, 201]
    }
  ]
};

option1 && myChart2.setOption(option1);








//end gradient graph xy
